package com.example.tp2_amar_ghemmour;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    private WineDbHelper databaseVins ;
    private  ListView listView;
    private  Cursor cursor;
    private SimpleCursorAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        databaseVins = new WineDbHelper(this);

        //insertion dans la DB
        databaseVins.populate();

        //recuperer les données depuis la DB
        cursor = databaseVins.fetchAllWines();
        cursor.moveToFirst();

        //lier la DB avec La listview
        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor,
                new String[]{
                        //Tableau de colonnes pour lier le curseur
                        WineDbHelper.COLUMN_NAME,
                        WineDbHelper.COLUMN_WINE_REGION
                },

                new int[]{
                        //Tableau de colonnes  auxquelles on va  lier de curseur
                        android.R.id.text1, android.R.id.text2
                },
                0);

        listView = (ListView) findViewById(R.id.listeVins);


        //l'option supprimer
        registerForContextMenu(listView);
        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                menu.add(Menu.NONE, 1, Menu.NONE, "supprimer");


            }
        });





        listView.setAdapter(adapter);

        //lancer la WineActivity depuis un item avec un intent
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                Intent intentWine = new Intent(view.getContext(), WineActivity.class);
                Cursor cursorWine = (Cursor) listView.getItemAtPosition(position);
                Wine wineS = databaseVins.cursorToWine(cursorWine);
                Log.d("Ouverture de WineA", wineS.toString());
                intentWine.putExtra("wineSelected", wineS);
                startActivity(intentWine);
            }
        });





        //le boutton flottant (+) pour ajouter un vin
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentWine = new Intent(view.getContext(), WineActivity.class);
                Log.d("ajout de Wine", "bien ajouté");
                intentWine.putExtra("wineSelected", (Bundle) null);
                startActivity(intentWine);
            }
        });



    }

    //recuperation de la position de l'item a supprimer

    @Override
    public boolean onContextItemSelected( MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        cursor.moveToPosition(info.position);
        databaseVins.deleteWine(cursor);
        cursor = databaseVins.fetchAllWines();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
    //mise a jour de la liste apres la suppression
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //mettre a jour l'activite apres l'operation
    @Override
    protected void onResume() {
        super.onResume();
        cursor = databaseVins.fetchAllWines();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

    }
}
