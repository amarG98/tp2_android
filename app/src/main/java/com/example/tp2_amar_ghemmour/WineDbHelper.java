package com.example.tp2_amar_ghemmour;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL("CREATE TABLE " + WineDbHelper.TABLE_NAME + " (" +
                WineDbHelper._ID + " INTEGER PRIMARY KEY," +
                WineDbHelper.COLUMN_NAME + " TEXT," +
                WineDbHelper.COLUMN_WINE_REGION + " TEXT," +
                WineDbHelper.COLUMN_LOC + " TEXT," +
                WineDbHelper.COLUMN_CLIMATE + " TEXT," +
                WineDbHelper.COLUMN_PLANTED_AREA + " TEXT,"+
                "UNIQUE (" + WineDbHelper.COLUMN_NAME + ", " + WineDbHelper.COLUMN_WINE_REGION+ ") ON CONFLICT ROLLBACK);" ) ;

        Log.d(TAG, "onCreate()");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSql =  "DROP TABLE IF EXISTS " + WineDbHelper.TABLE_NAME;
        db.execSQL(strSql);
        this.onCreate(db);
        Log.d(TAG, "onUpgrade()");

    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(WineDbHelper.COLUMN_NAME, wine.getTitle());
        values.put(WineDbHelper.COLUMN_WINE_REGION, wine.getRegion());
        values.put(WineDbHelper.COLUMN_LOC, wine.getLocalization());
        values.put(WineDbHelper.COLUMN_CLIMATE, wine.getClimate());
        values.put(WineDbHelper.COLUMN_PLANTED_AREA, wine.getPlantedArea());

        /*
         Inserting Row
         Insert the new row, returning the primary key value of the new row
        */
        long rowID = db.insert(WineDbHelper.TABLE_NAME, null, values);


        // call db.insert()
        db.close(); // Closing database connection

        Log.d(TAG, "addWine()");

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res=0;
        // updating row
        ContentValues values = new ContentValues();
        values.put(WineDbHelper.COLUMN_NAME, wine.getTitle());
        values.put(WineDbHelper.COLUMN_WINE_REGION, wine.getRegion());
        values.put(WineDbHelper.COLUMN_LOC, wine.getLocalization());
        values.put(WineDbHelper.COLUMN_CLIMATE, wine.getClimate());
        values.put(WineDbHelper.COLUMN_PLANTED_AREA, wine.getPlantedArea());
        // call db.update()
        res = db.update(TABLE_NAME, values, _ID + " = ? ", new String[] {Long.toString(wine.getId())});
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                WineDbHelper.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );


        if (cursor != null) {
            cursor.moveToFirst();
        }

        Log.d(TAG, "fetchAllWines()");

        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();

        // call db.delete();
        db.delete(TABLE_NAME, _ID + " = ?", new String[] {String.valueOf(cursor.getLong(0))});

        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = null;
        // build a Wine object from cursor
        int id_index = cursor.getColumnIndexOrThrow(_ID);
        int title_index = cursor.getColumnIndexOrThrow(COLUMN_NAME);
        int region_index = cursor.getColumnIndexOrThrow(COLUMN_WINE_REGION);
        int localization_index = cursor.getColumnIndexOrThrow(COLUMN_LOC);
        int climate_index = cursor.getColumnIndexOrThrow(COLUMN_CLIMATE);
        int plantedarea_index = cursor.getColumnIndexOrThrow(COLUMN_PLANTED_AREA);
        long id = (Long) cursor.getLong(id_index);
        String title = (String) cursor.getString(title_index);
        String region= (String) cursor.getString(region_index);
        String localization = (String) cursor.getString(localization_index);
        String climate = (String) cursor.getString(climate_index);
        String plantedarea = (String) cursor.getString(plantedarea_index);

        wine = new Wine (id, title, region, localization, climate, plantedarea);
        return wine;
    }
}

